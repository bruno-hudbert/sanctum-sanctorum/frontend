import { Injectable } from '@angular/core';
import { Project } from '../model/project';
import { Observable } from 'rxjs';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private heroesUrl: string = 'http://localhost:9081/projects';

  constructor(private http: HttpClient) {

  }

  getHeroes(): Observable<Project[]> {
    return this.http.get<Project[]>(this.heroesUrl)
  }
}
