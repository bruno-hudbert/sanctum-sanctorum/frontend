import {Component, OnInit} from '@angular/core';
import {ProjectService} from "./service/project.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  projects : any
  showFiller = false;
  title = 'Sanctum-Sanctorum';
  constructor(private projectService: ProjectService) {

  }
  ngOnInit(): void {
    this.projectService.getHeroes().subscribe(data => {
      this.projects = data;
    } )
  }

  login(ProjectForm: NgForm) {
    console.log(ProjectForm.value);
  }

  protected readonly navigator = navigator;
}
